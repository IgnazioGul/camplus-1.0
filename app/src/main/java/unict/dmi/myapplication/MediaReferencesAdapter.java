package unict.dmi.myapplication;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MediaReferencesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface ReferencesRecyclerViewCLickListener {
        void onClick(View v, int position);
        void onLongClick(View v, int position);
    }

    private ArrayList<Reference> mReferenceItems;
    private ReferencesRecyclerViewCLickListener[] listener; //INDEX 0 HAS PICTURE CLICK LISTENER, 1 FOR NOTE (SAME AS TYPE)

    public MediaReferencesAdapter(ArrayList<Reference> mReferenceItems, ReferencesRecyclerViewCLickListener[] listener) {
        this.mReferenceItems = mReferenceItems;
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        Reference currentRef = mReferenceItems.get(position);
        return currentRef.getType();
    }

    //inflate view for item
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //return different layout based on item type, 0 for PICTURES and 1 for NOTES
        View view;
        if(viewType==0) {
                    view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.picture_reference_item, parent, false);
                    return new MediaReferencesAdapter.PictureViewHolder(view);
        } else {
                     view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.note_reference_item, parent, false);
                    return new MediaReferencesAdapter.NoteViewHolder(view);
        }
    }

    //bind data from model to view
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Reference reference = mReferenceItems.get(position);
        // 0 is for picture, 1 for note
        if(reference.getType() == 0) {
            //get picture ref data from model
            PictureViewHolder pictureViewHolder = (PictureViewHolder) holder;
            pictureViewHolder.setmImageView(reference.getThumb());
            pictureViewHolder.setmImageTextView(reference.getPictureName());
        } else {
            NoteViewHolder noteViewHolder = (NoteViewHolder) holder;
            noteViewHolder.setNoteTextView(reference.getNote());
        }

    }

    @Override
    public int getItemCount() {
        return mReferenceItems.size();
    }


    //hold picture items info
    public class PictureViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private ImageView mImageView;
        private TextView mImageTextView;

        public PictureViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.pictureReferenceImageView);
            mImageTextView = (TextView) itemView.findViewById(R.id.pictureReferenceTextView);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void setmImageView(Bitmap bm) {
            this.mImageView.setImageBitmap(bm);
        }

        public void setmImageTextView(String description) {
            this.mImageTextView.setText(description);
        }

        @Override
        public void onClick(View v) {
            listener[0].onClick(v, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            listener[0].onLongClick(v, getAdapterPosition());
            return true;
        }
    }

    //hold note items info
    public class NoteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private TextView mNoteTextView;

        public NoteViewHolder(@NonNull View itemView) {
            super(itemView);
            mNoteTextView = (TextView) itemView.findViewById(R.id.noteTextView);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void setNoteTextView(String title) {
            this.mNoteTextView.setText(title);
        }

        @Override
        public void onClick(View v) {
            listener[1].onClick(v, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            listener[1].onLongClick(v, getAdapterPosition());
            return true;
        }
    }

}


