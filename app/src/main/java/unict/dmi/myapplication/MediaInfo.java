package unict.dmi.myapplication;

import android.database.Cursor;
import android.graphics.Bitmap;

public class MediaInfo {

    //represent video data ( as notes and imgs)
    private Bitmap mVideoThumbnail;
    private String mVideoName;  //actually has
    private int mPicturesCount;
    private int mNotesCount;
    private Cursor mVideoReferences; //holds references to video for later

    public MediaInfo(Bitmap videoThumbnail, String videoName, int picturesCount, int notesCount, Cursor videoReferences) {
        this.mVideoThumbnail = videoThumbnail;
        this.mVideoName = videoName;
        this.mPicturesCount = picturesCount;
        this.mNotesCount = notesCount;
        this.mVideoReferences = videoReferences;
    }

    public Cursor getmVideoReferences() {
        return mVideoReferences;
    }

    public Bitmap getmVideoThumbnail() {
        return mVideoThumbnail;
    }

    public void setVideoThumbnail(Bitmap videoThumbnail) {
        this.mVideoThumbnail = videoThumbnail;
    }

    public String getmVideoName() {
        return mVideoName;
    }

    public void setmVideoName(String mVideoName) {
        this.mVideoName = mVideoName;
    }

    public int getmPicturesCount() {
        return mPicturesCount;
    }

    public void setmPicturesCount(int mPicturesCount) {
        this.mPicturesCount = mPicturesCount;
    }

    public int getmNotesCount() {
        return mNotesCount;
    }

    public void setmNotesCount(int mNotesCount) {
        this.mNotesCount = mNotesCount;
    }

}
