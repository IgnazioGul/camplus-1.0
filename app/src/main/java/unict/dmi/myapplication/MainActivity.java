package unict.dmi.myapplication;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity  implements GestureDetector.OnGestureListener {

    //gesture data
    private float x1, x2, y1, y2;
    private GestureDetector mGestureDetector;
    private static int MIN_DISTANCE  = 200;

    private CameraDevice mCameraDevice;



    private CameraDevice.StateCallback mCameraDeviceStateCallback = new CameraDevice.StateCallback() {
        @RequiresApi(api = Build.VERSION_CODES.P)
        @Override
        //cameraDevice created, save it
        public void onOpened(@NonNull CameraDevice camera) {
            mCameraDevice = camera;
            mMediaRecorder = new MediaRecorder();
            if(isRecording) {
                try {
                    createVideoFileName();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mChronometer.setBase(SystemClock.elapsedRealtime());
                mChronometer.setVisibility(View.VISIBLE);
                mChronometer.start();
                startRecord();
                mMediaRecorder.start();
            }
            else {
                startPreview();
            }
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            camera.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            camera.close();
            mCameraDevice = null;
        }
    };
    private EditText mEditText;
    private ImageButton recordButton;
    private ImageButton pictureButton;
    private TextureView mTextureView;
    private TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        //width ed eight will be used on camera settings
        public void onSurfaceTextureAvailable(@NonNull SurfaceTexture surface, int width, int height) {
            setupCamera(width, height);
            connectCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(@NonNull SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(@NonNull SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(@NonNull SurfaceTexture surface) {

        }
    };
    private MyDatabaseHelper mDatabase;
    private File mVideoFolder;
    private String mVideoFileName;
    private File mImageFolder;
    private String mImageFileName;
    private boolean isRecording;
    private String mCameraId;  //holds rear camera ID
    private HandlerThread mBackgroundHandlerThread; //hold thread itself
    private Handler mBackgroundHandler;
    private static SparseIntArray ORIENTATIONS = new SparseIntArray();
    static{ //array orientations initialization
        ORIENTATIONS.append(Surface.ROTATION_0, 0);
        ORIENTATIONS.append(Surface.ROTATION_90, 90);
        ORIENTATIONS.append(Surface.ROTATION_180, 180);
        ORIENTATIONS.append(Surface.ROTATION_270, 270);

    }
    //request ID to catch event in callback
    private static final int REQUEST_CAMERA_PERMISSION_RESULT = 0;
    private static final int REQUEST_WRITE_EXTERNAL_PERMISSION_RESULT = 1;
    private static final int STATE_PREVIEW = 0;
    private static final int STATE_WAIT_LOCK = 1;
    private int mCaptureState = STATE_PREVIEW;
    private Size mPreviewSize;
    private Size mVideoSize;
    private Size mImageSize;
    private ImageReader mImageReader;
    private final ImageReader.OnImageAvailableListener mOnImageAvailableListener = new  //listener when user capture image
            ImageReader.OnImageAvailableListener() {
                @Override
                public void onImageAvailable(ImageReader reader) {
                    //callback called when imageReader captures an image, so we save it
                    mBackgroundHandler.post(new ImageSaver(reader.acquireLatestImage()));
                    Toast.makeText(MainActivity.this, "Immagine salvata", Toast.LENGTH_SHORT).show();
                }
            };

    //GESTURE DETECTION

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mGestureDetector.onTouchEvent(event);
        int action = event.getAction();
        // user start time swipe
        if(action == MotionEvent.ACTION_DOWN) {
            x1 = event.getX();
            y1 = event.getY();
        }
        //ending of time swipe
        else if(action == MotionEvent.ACTION_UP) {
            x2 = event.getX();
            y2 = event.getY();
            //get effective swipe distance
            float valX = x2 - x1;
            float valY = y2 - y1;
            if(Math.abs(valX) > MIN_DISTANCE) {
                //detect swipe right
                if(x2>x1) {
                    //Open MediaThumb activity
                    Intent intent = new Intent(this, MediaThumbActivity.class);
                    startActivity(intent);
                }
            }


        }
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }


    private class ImageSaver implements Runnable {
        private final Image mImage;
        public ImageSaver(Image img) {
            mImage = img;
        }

        @Override
        public void run() {
            ByteBuffer byteBuffer = mImage.getPlanes()[0].getBuffer();
            byte[] bytes = new byte[byteBuffer.remaining()];
            byteBuffer.get(bytes);

            FileOutputStream fileOutputStream = null;
            try {
                fileOutputStream = new FileOutputStream(mImageFileName);
                fileOutputStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                //clean up resources here
                mImage.close();
                //notify media store
                Intent mediaStoreUpdateIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                mediaStoreUpdateIntent.setData(Uri.fromFile(new File(mImageFileName)));
                sendBroadcast(mediaStoreUpdateIntent);
                if(fileOutputStream !=null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    private MediaRecorder mMediaRecorder;
    private Chronometer mChronometer;
    private int mTotalRotation;
    private CameraCaptureSession mPreviewCaptureSession; //camera session for image capture
    private CameraCaptureSession.CaptureCallback mPreviewCaptureCallback = new //callback to capture image in preview mode
            //intercept request to lock autofocus
            CameraCaptureSession.CaptureCallback() {
                private void process(CaptureResult captureResult) {
                    switch(mCaptureState) {
                        case STATE_PREVIEW:
                            //nothing to do
                            break;
                        case STATE_WAIT_LOCK:
                            mCaptureState = STATE_PREVIEW; //to stop getting pictures
                            Integer afState = captureResult.get(CaptureResult.CONTROL_AF_STATE);
                            if(afState == CaptureRequest.CONTROL_AF_STATE_FOCUSED_LOCKED) {
                                startStillCaptureRequest();
                                //save image!
                            }
                            break;
                    }
                }

                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                    process(result);
                    super.onCaptureCompleted(session, request, result);
                }
            };
    private CameraCaptureSession mRecordCaptureSession; //camera session for image capture
    private CameraCaptureSession.CaptureCallback mRecordCaptureCallback = new //callback to capture image in preview mode
            //intercept request to lock autofocus
            CameraCaptureSession.CaptureCallback() {
                private void process(CaptureResult captureResult) {
                    switch(mCaptureState) {
                        case STATE_PREVIEW:
                            //nothing to do
                            break;
                        case STATE_WAIT_LOCK:
                            mCaptureState = STATE_PREVIEW; //to stop getting pictures
                            Integer afState = captureResult.get(CaptureResult.CONTROL_AF_STATE);
                            if(afState == CaptureRequest.CONTROL_AF_STATE_FOCUSED_LOCKED) {
                                startStillCaptureRequest();
                                //save image!
                            }
                            break;
                    }
                }

                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                    process(result);
                    super.onCaptureCompleted(session, request, result);
                }
            };
    private CaptureRequest.Builder mCaptureRequestBuilder;
    public MainActivity() {
        isRecording = false;
    }

    //if TextureViw is not available, set listener to intercept the event (TextureView.available) anyway
    @Override
    protected void onResume() {
        super.onResume();
        startBackgroundThread();
        if(mTextureView.isAvailable())
        {
            setupCamera(mTextureView.getWidth(), mTextureView.getHeight()); //retrieve of camera ID
            connectCamera();
        }
        else
        {
            mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGestureDetector = new GestureDetector(MainActivity.this, this);
        setContentView(R.layout.activity_main);
        createVideoFolder();
        createImageFolder();
        mDatabase = MyDatabaseHelper.getDbInstance(this.getApplicationContext()); //DATABASE REFERENCE
        mMediaRecorder = new MediaRecorder();
        recordButton = (ImageButton) findViewById(R.id.recordButton);
        pictureButton = (ImageButton) findViewById(R.id.pictureButton);
        mEditText = (EditText) findViewById(R.id.noteEditText);
        mEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) { //if recording
                    String msg = v.getText().toString();
                    if (isRecording && !msg.isEmpty()) {
                       //SAVE TO DATABASE, YOU SURELY HAVE VIDEO NAME FILE
                        long millis = SystemClock.elapsedRealtime() - mChronometer.getBase();
                        String min = String.format("%02d:%02d:%02d",
                                TimeUnit.MILLISECONDS.toHours(millis),
                                TimeUnit.MILLISECONDS.toMinutes(millis) -
                                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                                TimeUnit.MILLISECONDS.toSeconds(millis) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                        mDatabase.insertVideoNote(getApplicationContext(), min, msg, mVideoFileName);
                        hideSoftKeyboard(MainActivity.this);
                        v.setText("");
                    }
                }
                return true;
            }
        });
        pictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lockFocus();
            }
        });
        mTextureView = (TextureView) findViewById(R.id.textureView);
        mChronometer = (Chronometer) findViewById(R.id.simpleChronometer);
        recordButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.P)
            @Override
            public void onClick(View v) {
                if(!isRecording)
                {   //user start recoridng, so app was not recording, when user clicks we start video capture
                    //this method also starts recording if permission is granted
                    isRecording = true;
                    recordButton.setImageResource(R.drawable.stop_recording);
                    checkWriteStoragePermission(); //ask for permission only when user starts recording
                }
                else
                { //app recording, when user clicks we stop video capture
                    mChronometer.stop();
                    mChronometer.setVisibility(View.INVISIBLE);
                    isRecording = false;
                    startPreview(); //preview must continue capturing anyway
                    recordButton.setImageResource(R.drawable.start_recording);
                    mMediaRecorder.stop();
                    mMediaRecorder.reset(); //reinitialize settings and create new file

                    //notify media store
                    Intent mediaStoreUpdateIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    mediaStoreUpdateIntent.setData(Uri.fromFile(new File(mVideoFileName)));
                    sendBroadcast(mediaStoreUpdateIntent);
                }

            }
        });
    }

    @Override
    protected void onPause() {
        closeCamera();
        stopBackgroundThread();
        super.onPause();
    }

    private void createVideoFolder()
    {
        File movieFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        mVideoFolder = new File( movieFile, "myVideoApp");  //create folder
        if(!mVideoFolder.exists()){
            mVideoFolder.mkdirs();
        }

    }

    private File createVideoFileName() throws IOException
    {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String prepend = "VIDEO_" + timestamp + "_";
        File videoFile = File.createTempFile(prepend, ".mp4", mVideoFolder);
        mVideoFileName = videoFile.getAbsolutePath(); //name
        return videoFile; //we do not create file here, but later when it's all configured
    }


    private void createImageFolder()
    {
        File imageFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        mImageFolder = new File( imageFile, "myVideoApp");  //create folder
        if(!mImageFolder.exists()){
            mImageFolder.mkdirs();
        }
    }

    private File createImageFileName() throws IOException
    {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String prepend = "IMAGE_" + timestamp + "_";
        File imageFile = File.createTempFile(prepend, ".jpg", mImageFolder);
        mImageFileName = imageFile.getAbsolutePath(); //name
        return imageFile; //we do not create file here, but later when it's all configured
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    private void checkWriteStoragePermission()
    {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
           //app has write permission
            try {
                createVideoFileName();

            } catch (IOException e) {
                e.printStackTrace();
            }
            startRecord();
            mMediaRecorder.start();
            mChronometer.setBase(SystemClock.elapsedRealtime());
            mChronometer.setVisibility(View.VISIBLE);
            mChronometer.start();
        }
        else {
                if(shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    Toast.makeText(this, "Questa app necessita del eprmesso per salvare i file", Toast.LENGTH_SHORT);
                }
                requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_PERMISSION_RESULT);
        }
    }

    private void setupMediaRecorder() throws IOException
    {
        //keep attention to the initialization order
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mMediaRecorder.setOutputFile(mVideoFileName);
        mMediaRecorder.setVideoEncodingBitRate(10000000);
        mMediaRecorder.setAudioEncodingBitRate(1220000);
        mMediaRecorder.setVideoFrameRate(30);
        mMediaRecorder.setAudioSamplingRate(12000);
        mMediaRecorder.setVideoSize(mVideoSize.getWidth(), mVideoSize.getHeight());
        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        mMediaRecorder.setOrientationHint(mTotalRotation);
        mMediaRecorder.prepare();
    }

    private void lockFocus()
    {
        mCaptureState = STATE_WAIT_LOCK;
        mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CaptureRequest.CONTROL_AF_TRIGGER_START);
        try {
            if (isRecording) { //specific capture request for image capture while recording
                mRecordCaptureSession.capture(mCaptureRequestBuilder.build(), mRecordCaptureCallback, mBackgroundHandler);
            } else {
                mPreviewCaptureSession.capture(mCaptureRequestBuilder.build(), mPreviewCaptureCallback, mBackgroundHandler);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    // get cameraId, adjust size for preview
    private void setupCamera(int width, int height)
    {
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        //take list of camera from cameraManager
        try {
            for(String cameraId : cameraManager.getCameraIdList())
            {
                //from characteristics we can detect if it's front or rear camera. We want to use rear one
                CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraId);
                if(cameraCharacteristics.get(cameraCharacteristics.LENS_FACING) == cameraCharacteristics.LENS_FACING_FRONT) {
                    continue;
                }
                StreamConfigurationMap map = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                //if portrait mode, swap width and height
                int deviceOrientation = getWindowManager().getDefaultDisplay().getRotation(); //current device orientation
                 mTotalRotation = sensorToDeviceRotation(cameraCharacteristics, deviceOrientation);
                boolean swapRotation = mTotalRotation == 90 || mTotalRotation == 270;
                int rotatedWidth = width;
                int rotatedHeight = height;
                if(swapRotation) //device is in portrait mode, need to swap widht/height
                {
                    rotatedHeight = width;
                    rotatedWidth = height;
                }
                //map returns a list of output supported for preview, we get best previewSize supported by sensor by checking
                // with actual widht height of TextureView (device screen) to match TextureVIew
                mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class), rotatedWidth, rotatedHeight);
                mVideoSize = chooseOptimalSize(map.getOutputSizes(MediaRecorder.class), rotatedWidth, rotatedHeight);
                mImageSize = chooseOptimalSize(map.getOutputSizes(ImageFormat.JPEG), rotatedWidth, rotatedHeight);
                mImageReader = ImageReader.newInstance(mImageSize.getWidth(), mImageSize.getHeight(), ImageFormat.JPEG, 1);
                mImageReader.setOnImageAvailableListener(mOnImageAvailableListener, mBackgroundHandler);
                mCameraId = cameraId;
                return;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CAMERA_PERMISSION_RESULT) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {//request rejected for camera
                Toast.makeText(this, "Permesso negato: CAMERA", Toast.LENGTH_SHORT).show();
            }
            if (grantResults[1] != PackageManager.PERMISSION_GRANTED) {//request rejected for audio
                Toast.makeText(this, "Permesso negato: AUDIO RECORDING", Toast.LENGTH_SHORT).show();
            }
        }
        else if( requestCode == REQUEST_WRITE_EXTERNAL_PERMISSION_RESULT) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    createVideoFileName();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else {
                Toast.makeText(this, "Permesso negato: WRITE EXTERNAL STORAGE", Toast.LENGTH_SHORT).show();

            }
        }
    }

    //connect to the camera and get object to interact with
    private void connectCamera()
    {
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        //USO API PER ANDROID 7, CHECK SE FUNZIONA SENZA
        try {
            //mCameraDeviceStateCallback will save camera object istance
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //android marshmallow request permission check, so do the check
                if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    cameraManager.openCamera(mCameraId, mCameraDeviceStateCallback, mBackgroundHandler);
                } else {
                    requestPermissions(new String[] {Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO}, REQUEST_CAMERA_PERMISSION_RESULT);
                }
            }
            else{
                //android <6.0, non servono permessi
                cameraManager.openCamera(mCameraId, mCameraDeviceStateCallback, mBackgroundHandler);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    private void startRecord()
    {
        try {
            setupMediaRecorder();
            SurfaceTexture surfaceTexture = mTextureView.getSurfaceTexture();
            surfaceTexture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            Surface previewSurface = new Surface(surfaceTexture);
            Surface recordSurface = mMediaRecorder.getSurface();
            mCaptureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
            mCaptureRequestBuilder.addTarget(previewSurface);
            mCaptureRequestBuilder.addTarget(recordSurface);
            //setup of CaptureRequestBuilder, then create capture session
            mCameraDevice.createCaptureSession(Arrays.asList(previewSurface, recordSurface, mImageReader.getSurface()),
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession session) {
                            mRecordCaptureSession = session;
                            try {
                                mRecordCaptureSession.setRepeatingRequest(mCaptureRequestBuilder.build(), null, null);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession session) {

                        }
                    }, null);
        } catch (IOException | CameraAccessException e) {
            e.printStackTrace();
        }
    }

    //get TextureView and convert it to SurfaceView, for the API. Then create and launch capture request
    private void startPreview()
    {
        SurfaceTexture surfaceTexture = mTextureView.getSurfaceTexture();
        surfaceTexture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
        Surface previewSurface = new Surface(surfaceTexture);
        try {
            mCaptureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mCaptureRequestBuilder.addTarget(previewSurface);
            mCameraDevice.createCaptureSession(Arrays.asList(previewSurface, mImageReader.getSurface()),
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession session) {
                            mPreviewCaptureSession = session;
                            try {
                                mPreviewCaptureSession.setRepeatingRequest(mCaptureRequestBuilder.build(), null, mBackgroundHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                            Toast.makeText(getApplicationContext(), "createCaptureSession failer", Toast.LENGTH_SHORT).show();
                        }
                    }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    //image capture session, setup captureBuilder and provide filename to save later
    private void startStillCaptureRequest()
    {
        try {
            if(isRecording) {
                //template for record session
                mCaptureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_VIDEO_SNAPSHOT); //capture a frame for the video
            } else {
                //template for preview session
                mCaptureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            }
            mCaptureRequestBuilder.addTarget(mImageReader.getSurface());
            mCaptureRequestBuilder.set(CaptureRequest.JPEG_ORIENTATION, mTotalRotation); //holds correct orientation
            //callback to provide file name before the capture
            CameraCaptureSession.CaptureCallback stillCaptureCallback = new
                    CameraCaptureSession.CaptureCallback() {
                        @Override
                        public void onCaptureStarted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, long timestamp, long frameNumber) {
                            super.onCaptureStarted(session, request, timestamp, frameNumber);
                            try {
                                createImageFileName();
                                if(isRecording){
                                    //save to database picture and video name with current minute:second
                                    long millis = SystemClock.elapsedRealtime() - mChronometer.getBase();
                                    String min = String.format("%02d:%02d:%02d",
                                            TimeUnit.MILLISECONDS.toHours(millis),
                                            TimeUnit.MILLISECONDS.toMinutes(millis) -
                                                    TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                                            TimeUnit.MILLISECONDS.toSeconds(millis) -
                                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                                    mDatabase.insertVideoPicture(getApplicationContext(), min, mImageFileName, mVideoFileName);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
            if(isRecording) {
                mRecordCaptureSession.capture(mCaptureRequestBuilder.build(), stillCaptureCallback, null);
            } else {
                mPreviewCaptureSession.capture(mCaptureRequestBuilder.build(), stillCaptureCallback, null);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    //free up resources, used within onPause
    private void closeCamera()
    {
        if(mCameraDevice != null)
        {
            mCameraDevice.close();
            mCameraDevice = null;
        }
    }

    private void startBackgroundThread()
    {
        mBackgroundHandlerThread = new HandlerThread("Camera2VideoImage"); //creating backgroundThread
        mBackgroundHandlerThread.start();
        mBackgroundHandler = new Handler(mBackgroundHandlerThread.getLooper()); //handler pointing the backgroundThread
    }

    private void stopBackgroundThread()
    {
        mBackgroundHandlerThread.quitSafely();
        try {
            mBackgroundHandlerThread.join();
            mBackgroundHandlerThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static int sensorToDeviceRotation(CameraCharacteristics cameraCharacteristics, int deviceOrientation)
    {
        int sensorOrientation = cameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION); //get current sensor orientation
        deviceOrientation = ORIENTATIONS.get(deviceOrientation); //get current device orientation
        return (sensorOrientation + deviceOrientation + 360) % 360;
    }

    //helper class to calculate minimus size for
    private static class CompareSizeByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            return Long.signum( (long)(lhs.getWidth() * lhs.getHeight()) -
                    (long)(rhs.getWidth() * rhs.getHeight()));
        }
    }

    private static Size chooseOptimalSize(Size[] choices, int width, int height)
    {
        //list for acceptable values from sensor
        List<Size> bigEnough = new ArrayList<Size>();
        //scan all preview resolution choices
        for(Size option : choices)
        {
            //check if aspect ratio matches TextureView
            //check if value for sensor is big enough for TextureView
            if(option.getHeight() >= option.getWidth() * height / width &&
                option.getWidth() >= width && option.getHeight() >= height)
                    bigEnough.add(option);
        }
        //get minimum closest size to match with
        if(bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizeByArea());
        } else{
            //actually it goes here (4000x3000)
            return choices[0];
        }
    }

    //utility to close keyboard
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        if(inputMethodManager.isAcceptingText()){
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(),
                    0
            );
        }
    }
}