package unict.dmi.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;


public class MediaThumbActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private ArrayList<MediaInfo> mMediaItems;
    private MyDatabaseHelper mDatabase;
    private MediaStoreAdapter mMediaStoreAdapter;
    private MediaStoreAdapter.RecyclerViewCLickListener mlistener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_thumb);
        mDatabase = MyDatabaseHelper.getDbInstance(this);

        mMediaItems = new ArrayList<MediaInfo>();
        //background data initialization on arrayList, otherwise UI blocks for 5/6 seconds
        new RecyclerViewInitializer().execute();
        setOnClickListener();
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mMediaStoreAdapter = new MediaStoreAdapter(mMediaItems, mlistener);
        mRecyclerView.setAdapter(mMediaStoreAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onDestroy() {
        //close cursor
        super.onDestroy();
    }

    private void setOnClickListener() {
        mlistener = new MediaStoreAdapter.RecyclerViewCLickListener() {
            @Override
            public void onClick(View v, int position) {
                String fileName = mMediaItems.get(position).getmVideoName();
                String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).toString() + "/myVideoApp/" + fileName;
                //START ACTIVITY AND MAYBE PASS DATA WITH INTENT
                Intent intent = new Intent(MediaThumbActivity.this, MediaPlayerActivity.class);
                intent.putExtra("videoUri", path);
                startActivity(intent);
            }
        };
    }

    private class RecyclerViewInitializer extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mMediaStoreAdapter.notifyDataSetChanged();
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Cursor files = getVideosFile();
            populateDataModels(files, mMediaItems);
            return null;
        }
    }

    //calling on activity bootstrap to get video files
    //get cursor from media store database (faster!) and not from reading storage
    private Cursor getVideosFile()
    {
        Uri collection;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            collection = MediaStore.Video.Media.getContentUri(MediaStore.VOLUME_EXTERNAL);
        } else {
            collection = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        }
        //getting from mediastore database, more efficient (really a lot more)
        Cursor mediaCursor = getContentResolver().query(
                collection,
                null,
                MediaStore.Video.Media.DATA + " like ? ", //DATA holds absolute filesystem path
                new String[] {"%myVideoApp%"},
                null
        );

        if(mediaCursor != null && mediaCursor.moveToFirst()) {
            String path = mediaCursor.getString(mediaCursor.getColumnIndex(MediaStore.Video.Media.DATA));
            System.out.println(path);
        }

        return mediaCursor;
    }

    //this populate recycler view data by querying on db
    private void populateDataModels(Cursor filesCursor,  ArrayList<MediaInfo> items)
    {
        if(filesCursor != null && filesCursor.getCount() > 0) {
            filesCursor.moveToFirst();
            ContentResolver cr = getContentResolver();
            int pathColIndex = filesCursor.getColumnIndex(MediaStore.Video.Media.DATA);
            int nameColIndex = filesCursor.getColumnIndex(MediaStore.Video.Media.DISPLAY_NAME);
            int videoIDColIndex = filesCursor.getColumnIndex(BaseColumns._ID);
            do {
                //get video path
                String videoPath = filesCursor.getString(pathColIndex);
                String fileName = filesCursor.getString(nameColIndex);
                long videoID = filesCursor.getLong(videoIDColIndex);
                Cursor c = mDatabase.getAllVideoReferences(videoPath);
                int nCount = 0, pCount = 0;

                //check if video has some reference on database
                if(c!=null && c.getCount() > 0 ) {
                    c.moveToFirst();
                    int typeColIndex = c.getColumnIndex(mDatabase.getColumnType());
                    //manipulate data if entry is relative to note or picture
                    do{
                        if (c.getString(typeColIndex).equals("IMG")) {
                            pCount++;
                            //get picture data
                        } else {
                            nCount++;
                        }
                    } while (c.moveToNext());
                }

                /* creating video thumbnail and testing performance
                Bitmap b = MediaStore.Video.Thumbnails.getThumbnail(cr, videoID, MediaStore.Video.Thumbnails.MINI_KIND, null);
               */
                MediaInfo newItem = new MediaInfo(null, fileName, pCount, nCount, c);
                items.add(newItem);
                int i = items.indexOf(newItem);
                //CREATING NEW THREAD TO LOAD ITEM THUMBNAIL, WE pass videoId, item index and items list
                new LoadVideoThumbnail().execute(cr, videoID, i, items);
            } while (filesCursor.moveToNext());
        }
    }

    private class LoadVideoThumbnail extends AsyncTask<Object, Void, Void> {

        int index;

        @Override
        protected void onPostExecute(Void aVoid) {
            mMediaStoreAdapter.notifyItemChanged(index);
            super.onPostExecute(aVoid);
        }

        //pass videoId, item index in item list (models) and items list itself
        @Override
        protected Void doInBackground(Object... objects) {
            ContentResolver cr = (ContentResolver) objects[0];
            Long videoID = (Long) objects[1]; //Long videoId
            index = (Integer) objects[2];  //index of item list
            ArrayList<MediaInfo> items = (ArrayList<MediaInfo>) objects[3]; //item list
            Bitmap thumb = MediaStore.Video.Thumbnails.getThumbnail(cr, videoID, MediaStore.Video.Thumbnails.MINI_KIND, null);
            items.get(index).setVideoThumbnail(thumb);
            return null;
        }
    }

}