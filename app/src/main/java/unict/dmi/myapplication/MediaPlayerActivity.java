package unict.dmi.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;


public class MediaPlayerActivity extends AppCompatActivity {

    private final static String PICTURE_BASE_PATH =  Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString() + "/myVideoApp/";

    private int PICTURE_REFERENCE_WIDTH;
    private int PICTURE_REFERENCE_HEIGHT;

    private PlayerView mPlayerView;
    private ProgressBar mProgressBar;
    private ImageView mBtFullscreen;
    private SimpleExoPlayer mSimpleExoPlayer;
    private boolean flag = false; //fullscreen flag (if true video is in landscape), used within fullscreen button click
    private RecyclerView mReferencesRecyclerView;
    private MediaReferencesAdapter mMediaReferencesAdapter;
    private ArrayList<Reference> mReferenceItems;
    private MyDatabaseHelper mDatabase;
    private MediaReferencesAdapter.ReferencesRecyclerViewCLickListener mPictureListener;
    private MediaReferencesAdapter.ReferencesRecyclerViewCLickListener mNoteListener;

    private Dialog mPictureDialog, mNoteDialog;
    private boolean isPictureDialogInit, isNoteDialogInit;

    //UTITLITIES
    //utility to conver dp to pixel
    public float dpToPixel(float dp) {
        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        float px = dp * (metrics.densityDpi/160f);
        return px;
    }

    //utility to rotate bitmap
    public static Bitmap RotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }


    //ACTIVITY LIFECYCLE
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_player);

        PICTURE_REFERENCE_WIDTH = (int) dpToPixel(84);
        PICTURE_REFERENCE_HEIGHT = (int) dpToPixel(71);

        isPictureDialogInit = false;
        isNoteDialogInit = false;

        mPlayerView = findViewById(R.id.playerView);
        mProgressBar = findViewById(R.id.progress_bar);
        mBtFullscreen = findViewById(R.id.bt_fullscreen);
        mReferencesRecyclerView = findViewById(R.id.refRecyclerView);
        mPictureDialog = new Dialog(this);
        mNoteDialog = new Dialog(this);

        mDatabase = MyDatabaseHelper.getDbInstance(this);
        mReferenceItems = new ArrayList<Reference>();
        new RecyclerViewReferenceInitializer().execute();
        setOnClickListener();
        mMediaReferencesAdapter = new MediaReferencesAdapter(mReferenceItems, new MediaReferencesAdapter.ReferencesRecyclerViewCLickListener[] {mPictureListener, mNoteListener});
        mReferencesRecyclerView.setAdapter(mMediaReferencesAdapter);
        mReferencesRecyclerView.setLayoutManager(new LinearLayoutManager(MediaPlayerActivity.this, LinearLayoutManager.HORIZONTAL, false));

        //get intent which started activity
        Intent intent = getIntent();
        //get video url from received intent
        Uri videoUri = Uri.parse(intent.getStringExtra("videoUri"));
        //make activity fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //init data source factory
        initializePlayer(videoUri);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //play video
        mSimpleExoPlayer.setPlayWhenReady(true);
        //get playback state
        mSimpleExoPlayer.getPlaybackState();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Stop video
        mSimpleExoPlayer.setPlayWhenReady(false);
        //get playback state
        mSimpleExoPlayer.getPlaybackState();
    }

    private Player.EventListener eventListener = new Player.EventListener() {
        @Override
        public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

        }

        @Override
        public void onLoadingChanged(boolean isLoading) {

        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            if(playbackState == Player.STATE_BUFFERING) {
                //when video is buffering we show progress bar
                mProgressBar.setVisibility(View.VISIBLE);
            }
            else if ( playbackState == Player.STATE_READY) {
                //else if video is ready we hide progress bar
                mProgressBar.setVisibility(View.GONE);
            }
        }

        @Override
        public void onRepeatModeChanged(int repeatMode) {

        }

        @Override
        public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {

        }

        @Override
        public void onPositionDiscontinuity(int reason) {

        }

        @Override
        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

        }

        @Override
        public void onSeekProcessed() {

        }
    };


    //METHODS

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        if(hasFocus) {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }

    private void initializePlayer(Uri videoUri) {
        if (mSimpleExoPlayer!=null)
            return;

            mSimpleExoPlayer= ExoPlayerFactory.newSimpleInstance(
                    new DefaultRenderersFactory(MediaPlayerActivity.this),
                    new DefaultTrackSelector(),
                    new DefaultLoadControl());
            mPlayerView.setPlayer(mSimpleExoPlayer);
            //keep screen on
            mPlayerView.setKeepScreenOn(true);
            mSimpleExoPlayer.addListener(eventListener);
            mSimpleExoPlayer.setPlayWhenReady(true); //CHECK, NEL VIDEO METTE A TRUE
            mSimpleExoPlayer.seekTo(0);

            MediaSource mediaSource=buildMediaSource(videoUri);
            mSimpleExoPlayer.prepare(mediaSource,true,false);
            mBtFullscreen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //set video to fullscreen
                    if(flag) {
                        //when flag is true we set enter_full_screen image
                        mBtFullscreen.setImageDrawable(getResources().getDrawable(R.drawable.ic_fullscreen));
                        //Set portrait orientation
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        flag = false;
                    } else {
                        //if flag is false we set exit_fullscreen image
                        mBtFullscreen.setImageDrawable(getResources().getDrawable(R.drawable.ic_fullscreen_exit));
                        //Set landscape orientation
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                        flag = true;
                    }
                }
            });
    }

    private MediaSource buildMediaSource(Uri uri){
        DataSource.Factory datasourceFactory = new DefaultDataSourceFactory(MediaPlayerActivity.this, Util.getUserAgent(MediaPlayerActivity.this,"Your App Name"));
        return new ExtractorMediaSource.Factory(datasourceFactory).createMediaSource(uri);
    }


    private class RecyclerViewReferenceInitializer extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mMediaReferencesAdapter.notifyDataSetChanged();
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Cursor references = getVideoReferences();
            populateDataModels(references, mReferenceItems);
            return null;
        }

        //get all video references (notes or images) by video_id
        private Cursor getVideoReferences()
        {
            //get all picture/note references for the video, get path from intent
            Intent intent = getIntent();
            String videoPath = intent.getStringExtra("videoUri");
            Cursor result = mDatabase.getAllVideoReferences(videoPath);
            return result;
        }

        private void populateDataModels(Cursor references, ArrayList<Reference> mReferenceItems) {
            if(references != null && references.getCount() > 0) {
                references.moveToFirst();
                //check type and populate item in data model list, same procedure for each query record
                int typeColIndex = references.getColumnIndex(mDatabase.getColumnType());
                do {
                    //get reference type
                    String referenceType = references.getString(typeColIndex);
                    if(referenceType.equals("IMG")) {
                        //Reference instance with picture data
                        int type = 0;
                        String picturePath = references.getString( references.getColumnIndex(mDatabase.getColumnPath()) );
                        String pictureName = picturePath.substring(picturePath.lastIndexOf("/")+1);
                        //get bitmap of img and resize such as it fit imageView
                        File imgFile = new File(picturePath);
                        Bitmap myBitmap = null, resizedBitmap = null;
                        if(imgFile.exists()){
                            myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                            resizedBitmap = Bitmap.createScaledBitmap(myBitmap, PICTURE_REFERENCE_WIDTH, PICTURE_REFERENCE_HEIGHT, true);
                            resizedBitmap = RotateBitmap(resizedBitmap, 90);
                        }
                        String[] time = references.getString( references.getColumnIndex(mDatabase.getColumnMin())).split(":");
                        Long millis = TimeUnit.HOURS.toMillis(Long.parseLong(time[0]))
                                + TimeUnit.MINUTES.toMillis(Long.parseLong(time[1]))
                                + TimeUnit.SECONDS.toMillis(Long.parseLong(time[2]));
                        Reference newPictureReference = new Reference(type, pictureName, null, resizedBitmap, millis);
                        mReferenceItems.add(newPictureReference);
                    }
                    //note case
                    else {
                            int type = 1;
                            String note = references.getString( references.getColumnIndex(mDatabase.getColumnNote()) );
                            String[] time = references.getString( references.getColumnIndex(mDatabase.getColumnMin())).split(":");
                            Long millis = TimeUnit.HOURS.toMillis(Long.parseLong(time[0]))
                                    + TimeUnit.MINUTES.toMillis(Long.parseLong(time[1]))
                                    + TimeUnit.SECONDS.toMillis(Long.parseLong(time[2]));
                            Reference newNoteReference = new Reference(type, null, note, null, millis);
                            mReferenceItems.add(newNoteReference);
                    }
                } while (references.moveToNext());
            }
        }
    }
    //SET ON CLICK LISTENERS FOR REFERENCES
    private void setOnClickListener() {
        mPictureListener = new MediaReferencesAdapter.ReferencesRecyclerViewCLickListener() {
            @Override
            public void onClick(View v, int position) {
                String pictureAbsPath = PICTURE_BASE_PATH + mReferenceItems.get(position).getPictureName();
                Bitmap image = null;
                try {
                    image = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(new File(pictureAbsPath)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                image = RotateBitmap(image, 90);

                openPictureDialog(image, pictureAbsPath); //we call inside mPictureDialog.show();
            }

            @Override
            public void onLongClick(View v, int position) {
                Reference currentItem = mReferenceItems.get(position);
                Long time = currentItem.getTime();
                mSimpleExoPlayer.seekTo(time);
            }
        };
        
        mNoteListener = new MediaReferencesAdapter.ReferencesRecyclerViewCLickListener() {
            @Override
            public void onClick(View v, int position) {
                String note = mReferenceItems.get(position).getNote();
                openNoteDialog(note);
            }

            @Override
            public void onLongClick(View v, int position) {
                Reference currentItem = mReferenceItems.get(position);
                Long time = currentItem.getTime();
                mSimpleExoPlayer.seekTo(time);
            }
        };
    }

    private void openPictureDialog(Bitmap image, String picturePath) {
        mSimpleExoPlayer.setPlayWhenReady(false);
        if(!isPictureDialogInit) {
            mPictureDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mPictureDialog.setContentView(R.layout.picture_window_layout);
            mPictureDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            isPictureDialogInit = true;
        }

        ImageButton btnBack = mPictureDialog.findViewById(R.id.pictureDialogBackImageButton);
        ImageButton btnFullScreen = mPictureDialog.findViewById(R.id.pictureDialogFullScreenImageButton);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPictureDialog.dismiss();
                mSimpleExoPlayer.setPlayWhenReady(true);
            }
        });

        btnFullScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //create intent, pass picture path to show it
                Intent intent = new Intent(MediaPlayerActivity.this, ShowPictureActivity.class);
                intent.putExtra("path", picturePath);
                startActivity(intent);
            }
        });
        ImageView picture = mPictureDialog.findViewById(R.id.pictureDialogImageView);
        picture.setImageBitmap(image);

        mPictureDialog.show();
    }

    private void openNoteDialog(String note) {
        mSimpleExoPlayer.setPlayWhenReady(false);
        if(!isNoteDialogInit) {
            mNoteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mNoteDialog.setContentView(R.layout.note_window_dialog);
            mNoteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            isNoteDialogInit = true;
        }
        TextView noteTextView = mNoteDialog.findViewById(R.id.noteTextView);
        noteTextView.setText(note);

        mNoteDialog.show();
    }
}