package unict.dmi.myapplication;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MediaStoreAdapter extends RecyclerView.Adapter<MediaStoreAdapter.ViewHolder> {

    private ArrayList<MediaInfo> mMediaItems;
    private RecyclerViewCLickListener listener;


    public MediaStoreAdapter(ArrayList<MediaInfo> mMediaItems, RecyclerViewCLickListener listener) {
        this.listener = listener;
        this.mMediaItems = mMediaItems;
    }

    //inflating item view from xml res
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.media_video_view, parent, false);
        return new ViewHolder(view);
    }

    //binding of data source and item view
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MediaInfo mediaInfo = mMediaItems.get(position);
        String description = "Snaphot: " + mediaInfo.getmPicturesCount() + "  Note: " + mediaInfo.getmNotesCount();

        holder.setmTitleTextView(mediaInfo.getmVideoName());
        holder.setmDescriptionTextView(description);
        holder.setmImageView(mediaInfo.getmVideoThumbnail());
    }

    @Override
    public int getItemCount() {
        if(mMediaItems == null)
            return 0;
        return mMediaItems.size();
    }

    public interface RecyclerViewCLickListener {
        void onClick(View v, int position);
    }

    //hold items info
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView mImageView;
        private TextView mDescriptionTextView;
        private TextView mTitleTextView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.mediastoreImageView);
            mTitleTextView = (TextView) itemView.findViewById(R.id.titleTextView);
            mDescriptionTextView = (TextView) itemView.findViewById(R.id.descriptionTextView);
            itemView.setOnClickListener(this);
        }

        public void setmImageView(Bitmap bm) {
            this.mImageView.setImageBitmap(bm);
        }

        public void setmDescriptionTextView(String description) {
            this.mDescriptionTextView.setText(description);
        }

        public void setmTitleTextView(String title) {
            this.mTitleTextView.setText(title);
        }

        @Override
        public void onClick(View v) {
            listener.onClick(v, getAdapterPosition());
        }
    }

}
