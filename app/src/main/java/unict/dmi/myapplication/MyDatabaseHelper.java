package unict.dmi.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.sql.Time;

public class MyDatabaseHelper extends SQLiteOpenHelper {

    private Context context;
    private static MyDatabaseHelper mDbHelperInstance = null;
    private static SQLiteDatabase mDbInstance = null;

    private static final String DATABASE_NAME= "MyCamera.db";
    private static final int DATABASE_VERSION= 1;

    private static final String TABLE_NAME= "video_references";
    private static final String COLUMN_ID= "id"; //id of each row, auto increment
    private static final String COLUMN_TYPE= "type";
    private static final String COLUMN_MIN = "minute"; //get time from timer while recording
    private static final String COLUMN_PATH= "path"; //if type=="IMG" the path is not null, follow it
    private static final String COLUMN_NOTE= "note";  //else note field will be not null, and contain the actual note
    private static final String COLUMN_VIDEO_ID= "video_id"; //guess it will be name



    public static int getDatabaseVersion() {
        return DATABASE_VERSION;
    }

    public static String getTableName() {
        return TABLE_NAME;
    }

    public static String getColumnId() {
        return COLUMN_ID;
    }

    public static String getColumnType() {
        return COLUMN_TYPE;
    }

    public static String getColumnMin() {
        return COLUMN_MIN;
    }

    public static String getColumnPath() {
        return COLUMN_PATH;
    }

    public static String getColumnNote() {
        return COLUMN_NOTE;
    }

    public static String getColumnVideoId() {
        return COLUMN_VIDEO_ID;
    }

    private MyDatabaseHelper(@Nullable Context context) {
        //Passing db name we create schema if it does'nt exists
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context=context;
    }

    //create table when database does not exists
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createQuery = "CREATE TABLE " + TABLE_NAME +
                " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_TYPE + " VARCHAR, "
                + COLUMN_MIN + " TIME, "
                + COLUMN_PATH + " VARCHAR, "
                + COLUMN_NOTE + " TEXT, "
                + COLUMN_VIDEO_ID + " VARCHAR);";
        db.execSQL(createQuery);
    }

    //to upgrade schema
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    //SINGLETON
    public static MyDatabaseHelper getDbInstance(Context ctx) {
        if (mDbHelperInstance == null) {
            mDbHelperInstance = new MyDatabaseHelper(ctx.getApplicationContext());
            mDbInstance = mDbHelperInstance.getWritableDatabase();
        }

        return mDbHelperInstance;
    }

    //helper to insert text note related to video at current minute
    public void insertVideoNote(Context ctx, String time, String note, String videoId)
    {
        //if this method is called we surely have an instance of db (mDbInstance != null)
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_TYPE, "NOTE");
        cv.put(COLUMN_MIN, time);
        //PATH COLUMN WILL BE NULL (Because we insert a textual note)
        cv.put(COLUMN_NOTE, note);
        cv.put(COLUMN_VIDEO_ID, videoId);

        long result = mDbInstance.insert(TABLE_NAME, null, cv);
        if(-1==result) {
            Toast.makeText(this.context, "insertVideoNote FAILED", Toast.LENGTH_SHORT).show(); //show error toast
        } else {
            Toast.makeText(this.context, "insertVideoNote SUCCESS", Toast.LENGTH_SHORT).show(); //show error toast
        }
    }

    //helper to insert snapshot picture related to video at current minute
    public void insertVideoPicture(Context ctx, String time, String path, String videoId)
    {
        //if this method is called we surely have an instance of db (mDbInstance != null)
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_TYPE, "IMG");
        cv.put(COLUMN_MIN, time);
        //NOTE COLUMN WILL BE NULL (Because we insert a picture path for later)
        cv.put(COLUMN_PATH, path);
        cv.put(COLUMN_VIDEO_ID, videoId);

        long result = mDbInstance.insert(TABLE_NAME, null, cv);
        if(-1==result) {
            Toast.makeText(this.context, "insertVideoPicture FAILED", Toast.LENGTH_SHORT).show(); //show error toast
        }
    }

    //get all video references (notes or images) by video_id
    public Cursor getAllVideoReferences(String videoId)
    {
        //get all picture/note references for the video
        Cursor result = mDbInstance.query(TABLE_NAME, new String[] {COLUMN_VIDEO_ID, COLUMN_TYPE, COLUMN_MIN, COLUMN_PATH, COLUMN_NOTE},
                "video_id = ?", new String[] {videoId}, null, null, null, null);
        return result;
    }
}
