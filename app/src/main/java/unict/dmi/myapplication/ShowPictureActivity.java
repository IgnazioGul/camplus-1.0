package unict.dmi.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;

public class ShowPictureActivity extends AppCompatActivity {

    private ImageView pictureImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_picture);

        pictureImageView = findViewById(R.id.pictureImageView);
        Intent intent = getIntent();
        String picturePath = intent.getStringExtra("path");
        Bitmap image = null;
        try {
            image = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(new File(picturePath)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        image = MediaPlayerActivity.RotateBitmap(image, 90);

        pictureImageView.setImageBitmap(image);
    }
}