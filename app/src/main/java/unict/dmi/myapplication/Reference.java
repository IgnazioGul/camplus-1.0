package unict.dmi.myapplication;

import android.graphics.Bitmap;

//hold model
public class Reference {
    private int type; // 0 for pictures, 1 for notes
    private String pictureName; //null if(type==1)
    private String note; //null if(type==0)
    private Bitmap thumb; //null if (note==1)
    private Long time; //time position in video
    public Bitmap getThumb() {
        return thumb;
    }

    public Reference(int type, String pictureName, String note, Bitmap t, Long time) {
        this.type = type;
        this.time = time;
        if(type==0) {
            this.pictureName = pictureName;
            this.thumb = t;
            this.note = null;
        } else {
            this.pictureName = null;
            this.note = note;
        }
    }

    public Long getTime() {
        return time;
    }

    public int getType() {
        return type;
    }

    public String getPictureName() {
        return pictureName;
    }

    public String getNote() {
        return note;
    }
}